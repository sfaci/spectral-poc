# Spectral PoC

This is a Proof of Concept about how to apply spectral ruleset to a OpenAPI definition to validate it

## Getting started

### Sample ruleset configuration file

## How to run

### Default running

### Formatting results

### Show only the errors

## References

- OAS built-in rules: https://docs.stoplight.io/docs/spectral/4dec24461f3af-open-api-rules
- Core functions: https://docs.stoplight.io/docs/spectral/cb95cf0d26b83-core-functions 
- How to create a custom rule: https://docs.stoplight.io/docs/spectral/d3482ff0ccae9-rules  
- Recommended flag: https://docs.stoplight.io/docs/spectral/0a73453054745-recommended-or-all 
- Spectral CLI reference: https://docs.stoplight.io/docs/spectral/9ffa04e052cc1-spectral-cli 
- Custom sample rulesets: https://github.com/stoplightio/spectral-rulesets 